import type { RouteRecordRaw } from 'vue-router'
import ListingsView from '@/views/ListingsView.vue'
import AboutView from '@/views/AboutView.vue'

/**
 * Routes used exclusively by the menu, each associated with specific images.
 */
export const menuRoutes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Houses',
    component: ListingsView,
    meta: {
      image: '/images/ic_mobile_navigarion_home@3x.png',
      activeImage: '/images/ic_mobile_navigarion_home_active@3x.png'
    }
  },
  {
    path: '/about',
    name: 'About',
    component: AboutView,
    meta: {
      image: '/images/ic_mobile_navigarion_info@3x.png',
      activeImage: '/images/ic_mobile_navigarion_info_active@3x.png'
    }
  }
]
