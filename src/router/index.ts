import { createRouter, createWebHistory } from 'vue-router'
import type { RouteRecordRaw } from 'vue-router'
import ListingsView from '@/views/ListingsView.vue'
import AboutView from '@/views/AboutView.vue'
import CreateListingView from '@/views/CreateListingView.vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Houses',
    component: ListingsView
  },
  {
    path: '/about',
    name: 'About',
    component: AboutView
  },
  {
    path: '/create',
    name: 'Create Listing',
    component: CreateListingView
  }
]

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes as RouteRecordRaw[]
})

export default router