import type { Listing } from '@/types/Listing'
import { sortingOptions } from '@/data/sorting-options'

const apiKey = import.meta.env.VITE_API_KEY
const apiUrl = import.meta.env.VITE_API_URL

const headers = new Headers()
headers.append('X-Api-Key', apiKey)

/**
 * Fetches a list of listings with optional sorting and searching.
 * @param sort - The sorting method (e.g., 'price' or 'size').
 * @param search - The search query to filter listings.
 * @returns A Promise that resolves to an array of Listing objects.
 */
export function getAllListings(sort: string, search: string): Promise<Listing[]> {
  return fetch(`${apiUrl}/houses`, {
    method: 'GET',
    headers: headers
  })
    .then((res) => {
      if (!res.ok) {
        throw new Error(`Request failed with status ${res.status}`)
      }
      return res.json()
    })
    .then((listings: Listing[]) => {
      if (sort) {
        listings = sortListings(listings, sort)
      }

      if (search) {
        listings = filterListings(listings, search)
      }
      return listings
    })
}

/**
 * Sorts a list of listings based on the provided sorting method.
 * @param listings - The array of Listing objects to be sorted.
 * @param sort - The sorting method (e.g., 'price' or 'size').
 * @returns A sorted array of Listing objects.
 */
function sortListings(listings: Listing[], sort: string): Listing[] {
  if (sort === sortingOptions[1].value) {
    listings.sort((a, b) => {
      return b.size - a.size
    })
  } else if (sort === sortingOptions[0].value) {
    listings.sort((a, b) => {
      return b.price - a.price
    })
  }

  return listings
}

/**
 * Filters a list of listings based on a search query.
 * @param listings - The array of Listing objects to be filtered.
 * @param search - The search query.
 * @returns A filtered array of Listing objects.
 */
function filterListings(listings: Listing[], search: string): Listing[] {
  const searchQuery = search.toLowerCase().trim()

  return listings.filter((listing) => {
    const description = listing.description.toLowerCase()
    const city = listing.location.city.toLowerCase()
    const street = listing.location.street.toLowerCase()

    return (
      description.includes(searchQuery) ||
      city.includes(searchQuery) ||
      street.includes(searchQuery)
    )
  })
}

/**
 * Fetches a single listing by its ID.
 * @param id - The ID of the listing to retrieve.
 * @returns A Promise that resolves to a Listing object.
 */
export function getListingById(id: number): Promise<Listing> {
  return fetch(`${apiUrl}/houses/${id}`, {
    method: 'GET',
    headers: headers
  }).then((res) => {
    if (!res.ok) {
      throw new Error(`Request failed with status ${res.status}`)
    }
    return res.json()
  })
}

/**
 * Creates a new listing using form data.
 * @param formData - The FormData object containing listing data.
 * @returns A Promise that resolves to the created Listing object.
 */
export function createListing(formData: FormData): Promise<Listing[]> {
  return fetch(`${apiUrl}/houses`, {
    method: 'POST',
    headers: headers,
    body: formData
  }).then((res) => {
    if (!res.ok) {
      throw new Error(`Request failed with status ${res.status}`)
    }
    return res.json()
  })
}

/**
 * Deletes a listing by its ID.
 * @param id - The ID of the listing to delete.
 * @returns A Promise that resolves to the deleted Listing object.
 */
export function deleteListing(id: number): Promise<Listing[]> {
  return fetch(`${apiUrl}/houses/${id}`, {
    method: 'DELETE',
    headers: headers
  }).then((res) => {
    if (!res.ok) {
      throw new Error(`Request failed with status ${res.status}`)
    }
    return res.json()
  })
}
