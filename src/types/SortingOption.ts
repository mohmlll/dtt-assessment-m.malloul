export interface SortingOption {
  label: string
  value: string
}
