import type { SortingOption } from '@/types/SortingOption'

export const sortingOptions: SortingOption[] = [
  { label: 'Price', value: 'price' },
  { label: 'Size', value: 'size' }
]
