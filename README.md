# DTT-assessment-m-malloul

### Setup
To set up this project, follow these steps:

Create a .env file by making a copy of `.env.example`\
Add the required information to the `.env` file.

```
VITE_API_KEY=ncPz3wmE8eGvTQsh4WF_fLjXDiu-g95R
VITE_API_URL=https://api.intern.d-tt.nl/api
```


### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests

```sh
npm run test:unit
```
No tests available yet

### Lint

```sh
npm run lint
```

### type check

```sh
npm run type-check
```
