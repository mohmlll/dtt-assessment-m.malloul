/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#EB5440',
        secondary: '#4A4B4C',
        tertiary: '#E8E8E8',
        quaternary: '#C3C3C3',
        background1: '#F6F6F6',
        background2: '#FFFFFF'
      },
      textColor: {
        primary: '#000000',
        secondary: '#4A4B4C',
        hyperlink: '#067BC2'
      },
      fontFamily: {
        montserrat: ['Montserrat', 'sans-serif'],
        openSans: ['Open Sans', 'sans-serif'],
      },
      fontSize: {
        header1: '32px',
        header1Mobile: '18px',

        header2: '22px',
        header2Mobile: '14px',

        activeMenu: '18px',

        inactiveMenuMobile: '14px',
        inactiveMenu: '18px',

        buttons: '18px',
        buttonsMobile: '14px',
    
        backButton: '16px',

        inputFieldTitle: '14px',
        inputFieldTitleMobile: '12px',

        emptyStateMessage: '18px',
        emptyStateMessageMobile: '14px',

        errorMessage: '14px',
        errorMessageMobile: '12px',

        listingInformation: '16px',
        listingInformationMobile: '12px',

        bodyText: '18px',
        bodyTextMobile: '12px',

        inputField: '14px',
        inputFieldMobile: '12px'
      },
      backgroundImage: {
        'house': "url('/images/img_background@3x.png')",
      },
    },
  },
  plugins: []
}
